import React, { useState, useEffect } from "react";
import { useAuth0 } from '@auth0/auth0-react';

const Auth = () => {
  const {
    user,
    isAuthenticated,
    isLoading,
    getAccessTokenSilently,
    loginWithPopup,
    logout
  } = useAuth0();
  
  const [accessToken, setAccessToken] = useState();

  useEffect(() => {
    getAccessTokenSilently()
      .then(response => {
        setAccessToken(response);
      });
  }, [user]);

  const goCommunity = () => {

  }

  const goPreferences = () => {
    
  }

  const login = () => {
    loginWithPopup();
  }

  const logoutCustom = () => {
    logout({returnTo: 'http://localhost:8000/'});
  }

  if(isLoading) {
    return ('...');
  }

  return (
  <>
    {
        isAuthenticated ? (
            <>
                <span className="hide-1122">
                    <div className="d-flex align-items-center">
                        <button className="btn btn-outline-primary px-6 font-bold" onClick={() => goCommunity()}>Community</button>
                    
                        <div className="dropdown dropdown-right line-1-0 ml-3">
                            <div className="dropdown-toggle user-holder line-1-0"><span>{ user.name }</span></div>
                            <ul className="menu menu-right">
                                <li className="menu-item">
                                    <button className="btn btn-outline-transparent w-100 text-left" onClick={logoutCustom}>Logout</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </span>
                <span className="display-1122 text-center">
                    <div className="w-100">
                        <button className="btn btn-outline-primary px-3 font-bold" onClick={() => goCommunity()}>Community</button>
                        <button onClick={() => goPreferences()} className="btn btn-primary font-bold px-6 ml-3" >Start now</button>
                    </div>
                
                    <div className="dropdown dropdown-right line-1-0 mt-3">
                        <div className="dropdown-toggle user-holder line-1-0"><span>{ user.name }</span></div>
                        <ul className="menu menu-right">
                            <li className="menu-item">
                                <button className="btn btn-outline-transparent w-100 text-left" onClick={logoutCustom}>Logout</button>
                            </li>
                        </ul>
                    </div>
                </span>
          </>
        ) : (
            <>
                <span className="hide-1122">
                    <button className="btn btn-outline-primary px-6 font-bold ml-12" onClick={() => login()}>Sign in</button>
                    <button onClick={() => goPreferences()} className="btn btn-primary font-bold px-6 ml-3" >Start now</button>
                </span>
                <span className="display-1122">
                    <button className="btn btn-outline-primary px-6 font-bold" onClick={() => login()}>Sign in</button>
                    <button onClick={() => goPreferences()} className="btn btn-primary font-bold px-6 ml-3" >Start now</button>
                </span>
            </>
        )

    }
  </>
  );
}

export default Auth;
