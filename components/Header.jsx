import React, { useEffect } from "react"
import Link from 'next/link';
import { useTranslation, i18n } from 'next-i18next';
import { useRouter } from 'next/router';
import axios from "axios";
import Auth from './Auth';
import MobileMenu from './MobileMenu';
import Head from 'next/head'

const Header = (props) => {
    const { t, i18n } = useTranslation();
    const router = useRouter();

    return (
        <div>
            <Head>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <span className="display-1122">
                <>
                    <MobileMenu>
                        <Link href="/">
                            <img src="/assets/svg/logo.svg" alt="" className="navbar-brand mr-5" />
                        </Link>

                        {/* <div className="mt-12 text-center">
                            <Auth />
                        </div> */}

                        <ul className="mt-12">
                            <li className="mobile-link-1"><Link href="/team"><a className="nav-item ml-5">Команда</a></Link></li>
                        </ul>
                    </MobileMenu>
                </>
            </span>
            <span className="hide-1122">
                <header className="py-6" id="navbar">
                    <div className="container navbar">
                        <section className="navbar-section">
                            <Link href="/">
                                <img src="/assets/svg/logo.svg" alt="" className="navbar-brand mr-5" />
                            </Link>
                            <Link href="/team"><a className="nav-item ml-5">Команда</a></Link>
                        </section>
                        <section className="navbar-section">
                            {/* <Auth /> */}
                            <div className="dropdown dropdown-right mx-4 d-none">
                                <div className="btn btn-link dropdown-toggle font-bold">
                                <img src="/globe.svg" className="locale_icon" alt=""/>
                                </div>
                            </div>
                        </section>
                    </div>
                </header>
            </span>
        </div>
    )
}

export default Header;