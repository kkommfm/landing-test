import React from "react";
import Link from 'next/link';
import axios from 'axios';
import * as EmailValidator from 'email-validator';
// import { sendLog } from '../utils/helpers';
import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';
import CookieConsent from "react-cookie-consent";

const Footer = (props) => {
    // const { t } = useTranslation();
    const router = useRouter();

    return (
        <footer className="footer">
            <div className="footer-upper">
                <div className="container">
                    <div className="h1">Enjoy being a founder!</div>
                
                    <p className="font-half">Создавайте прорывные продукты и оставайтесь на передовой цифровой трансформации. Используйте наш ИИ для изучения продуктов лидеров рынка. Добавьте что-то от себя и получите конкурентное преимущество</p>
                </div>
            </div>
            <div className="footer-lower">
                <div className="container">
                    <div className="columns">
                        <div className="column col-4 col-md-12 col-sm-12">
                            <h4 className="text-uppercase">О нас</h4>

                            <div className="text-muted">Автоматизированный Менеджер Продукта для фаундеров на основе ИИ, который поможет вам преобразовать идею в стартап</div>

                            <div className="my-6">
                                <a href="https://www.ccifr.ru/ward/400_member_companies?formcompany=upfinity&formcategory=0&formsector=0" target="_blank" rel="noreferrer">
                                    <img src="/frtpp.svg" alt="frtpp" style={{maxWidth: '250px' }} />
                                </a>

                                <div className="mt-3">
                                    <small className="text-muted d-inline-block line-1">Мы являемся компанией-членом Франко-Российской Торгово-Промышленной Палаты</small>
                                </div>
                            </div>
                        </div>
                        <div className="column col-1 hide-md"></div>
                        <div className="column col-7 col-md-12 col-sm-12 mt-12-mobile">
                            <h4 className="text-uppercase">Меню</h4>

                            <div className="navbar">
                                <section className="navbar-section flex-wrap-wrap">
                                    <Link href="/legal/rules" locale={router.locale || 'ru'}><a className="btn btn-link-ligth font-bold mr-4 pl-0">Правила</a></Link>
                                    <Link href="/legal/confidential" className="btn btn-link-ligth font-bold mr-4 pl-0"><a className="btn btn-link-ligth font-bold mr-4 pl-0">Политика конфиденциальности</a></Link>
                                    <Link href="/legal/services" className="btn btn-link-ligth font-bold mr-4 pl-0"><a className="btn btn-link-ligth font-bold mr-4 pl-0">Сервисы и Тарифы</a></Link>
                                </section>
                            </div>
                        </div>
                        <div className="column col-12 col-md-12 mt-4"></div>

                        
                        <div className="hide-1122 column col-4 col-md-12 col-sm-12 mb-5 pb-5">
                            <h4>Есть вопросы? Задайте их.</h4>

                       

                            <a href={`mailto:feedback@upfinity.io`}><img className="locale_icon mr-4" src="/email_icon.png" alt="Upfinity email"/></a>
                            <a rel="noopener noreferrer" target="_blank" href="https://www.facebook.com/upfinity"><img className="locale_icon mr-4" src="/fb_icon.png" alt="Upfinity facebook"/></a>
                            <a rel="noopener noreferrer" target="_blank" href="https://www.linkedin.com/company/upfinity"><img className="locale_icon" src="/linked_icon.png" alt="Upfinity linkedin"/></a>
                        </div>
                        <div className="column col-1"></div>
                        <div className="display-1122 text-center column col-7 col-md-12">
                            

                            <div className="mt-12 text-right text-center-mobile text-muted">© 2021 Upfinity, LLC | Сделано с любовью в Москве</div>

                            <div className="text-right text-center-mobile">
                                <small>
                                    <a href="#" className="text-muted no-link">ОГРНИП 321774600061797 | ИНН 540822801796</a>
                                </small>
                            </div>
                        </div>
                        <div className="hide-1122 column col-7 col-md-12">
                          

                            <div className="mt-12 text-right text-center-mobile text-muted">© 2021 Upfinity, LLC | Сделано с любовью в Москве</div>

                            <div className="text-right text-center-mobile">
                                <small>
                                    <a href="#" className="text-muted no-link">ОГРНИП 321774600061797 | ИНН 540822801796</a>
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <CookieConsent
                location="bottom"
                buttonText="Скрыть"
                cookieName="cookie-consent"
                style={{ background: "#121212" }}
                buttonWrapperClasses="button-cookie-consent"
                buttonStyle={{ background: "#fff", color: "#4e503b", fontSize: "13px" }}
                expires={150}
                >
                <div>Этот сайт использует куки для улучшения вашего пользовательского опыта. Продолжая работу с этим сайтом, вы принимаете <Link href='/legal/rules' >Пользовательское соглашение</Link>, <Link href='/legal/confidential'>Политику конфиденциальности</Link> и <Link href='/legal/services'>Сервисы и Тарифы</Link></div>
            </CookieConsent>
        </footer>
    )
}
  
export default Footer;