/**
 * SEO component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import React, { useState, useEffect } from "react"
import PropTypes from "prop-types"
import { Helmet } from "react-helmet"
import { useStaticQuery, graphql } from "gatsby"

const SEO = ({ description, lang, meta, title, date }) => {
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            description
            social {
              twitter
            }
          }
        }
      }
    `
  )

  const metaDescription = description || site.siteMetadata.description
  const metaDate = date || '2021-06-17T14:14:26.680Z'
  const defaultTitle = site.siteMetadata?.title

  return (
    <Helmet
      htmlAttributes={{
        lang,
      }}
      title={title}
      titleTemplate={defaultTitle ? `%s | ${defaultTitle}` : null}
      meta={[
        {
          name: `yandex-verification`,
          content: `2ac426bddf5b4def`
        },
        {
          name: `description`,
          content: metaDescription,
        },
        {
          property: `og:url`,
          content: 'https://upfinity.io',
        },
        {
          property: `og:image`,
          content: 'https://upfinity.io/svg_1.png',
        },
        {
          property: `og:title`,
          content: title,
        },
        {
          property: `og:description`,
          content: metaDescription,
        },
        {
          property: `og:type`,
          content: `website`,
        },
        {
          name: `twitter:card`,
          content: `summary`,
        },
        {
          name: `twitter:creator`,
          content: site.siteMetadata?.social?.twitter || ``,
        },
        {
          name: `twitter:title`,
          content: title,
        },
        {
          name: `twitter:description`,
          content: metaDescription,
        },
        {
          name: 'author',
          content: 'Danila Ivanchenko'
        },
        {
          name: 'datePublished',
          content: metaDate
        },
        {
          name: 'image',
          content: 'https://upfinity.io/svg_1.png'
        },
        {
          name: 'publisher',
          content: 'Danila Ivanchenko'
        },
        {
          name: 'google-site-verification',
          content: 'Tvxd6jNR4LjL8uZiYdi3YQNtB18AxUvlo_zvjvHO1To'
        },
        {
          name: 'viewport',
          content: 'width=device-width, initial-scale=1, shrink-to-fit=no'
        }
      ].concat(meta)}
    >
      
    </Helmet>
  )
}

SEO.defaultProps = {
  lang: `en`,
  meta: [],
  description: ``,
}

SEO.propTypes = {
  description: PropTypes.string,
  lang: PropTypes.string,
  meta: PropTypes.arrayOf(PropTypes.object),
  title: PropTypes.string.isRequired,
}

export default SEO
