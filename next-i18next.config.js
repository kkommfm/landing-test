const path = require('path');

module.exports = {
  i18n: {
    // These are all the locales you want to support in
    // your application
    locales: ['ru', 'en'],
    // This is the default locale you want to be used when visiting
    // a non-locale prefixed path e.g. `/hello`
    defaultLocale: 'ru',
    realDefaultLocale: 'ru',
    fallbackLocale: ["ru"],
    fallbackLng: ["ru"],
    ns: ['common'],
    // serializeConfig: false,
    // This is a list of locale domains and the default locale they
    // should handle (these are only required when setting up domain routing)
    // Note: subdomains must be included in the domain value to be matched e.g. "fr.example.com".
    localeDetection: false,
    localePath: path.resolve('./public/locales'),
    useSuspense: false,
  },
  localePath: path.resolve('./public/locales'),
};