import React, { useEffect, useState } from 'react';
import axios from "axios";
import Head from 'next/head';
import Link from 'next/link';
import Layout from "../components/layout";
// import { useTranslation, i18n } from 'next-i18next';
import { useRouter } from 'next/router';
import Modal from 'react-modal';
// import nextI18NextConfig from '../next-i18next.config.js';

// import { serverSideTranslations } from 'next-i18next/serverSideTranslations'


Modal.setAppElement('#__next');

const modalStyles = {
  overlay: {
    backgroundColor: "rgba(0, 0, 0, 0.58)",
  },
  content: {
    position: "relative",
    top: "auto",
    left: "auto",
    right: "auto",
    bottom: "auto",
    maxWidth: "960px",
    margin: "32px auto",
    padding: 0,
    border: 0,
    zIndex: 999
  },
};

// export const getStaticProps = async ({ locale }) => ({
//   props: {
//     ...(await serverSideTranslations(locale, ['common'], nextI18NextConfig)),
//   },
// })

export default function Team() {
  // const { t } = useTranslation();
  const router = useRouter();
  const [members, setMembers] = useState([
    {
      hash: '#denis-anoshin',
      ru: {
        name: 'Денис Аношин',
        title: 'Со-основатель,  CEO',
        photo: `../../anoshin.png`,
        color: `rgba(17, 175, 164, 0.15)`,
        contacts: `../../vcf/rus-denis.vcf`,
        details: {
          title: 'Со-основатель, CEO',
          links: [
            {
              icon: '../../linkedin.png',
              url: 'https://www.linkedin.com/in/denis-anoshin-cfa-428b2b49/'
            },
            {
              icon: '../../fb.png',
              url: 'https://www.facebook.com/denis.anoshin'
            }
          ],
          about: 'В качестве генерального директора Денис играет важную роль в стратегическом и операционном управлении компанией. Кроме того, он лично возглавляет проект по разработке критически важного модуля ИИ. Денис обладает 10-летним опытом в инвестиционном банкинге и 4-летним опытом в консалтинге. Недавно он получил степень Executive MBA в HEC, Париж.'
        }
      },
      en: {
        name: 'Denis Anoshin',
        title: 'Co-founder, CEO',
        photo: `../../anoshin.png`,
        color: `rgba(17, 175, 164, 0.15)`,
        contacts: `../../vcf/eng-denis.vcf`,
        details: {
          title: 'Co-founder, CEO',
          links: [
            {
              icon: '../../linkedin.png',
              url: 'https://www.linkedin.com/in/denis-anoshin-cfa-428b2b49/'
            },
            {
              icon: '../../fb.png',
              url: 'https://www.facebook.com/denis.anoshin'
            }
          ],
          about: 'As an acting CEO Denis plays important role both in strategic and operational management of the firm. In addition he personally leads development of the crucial AI module. Denis has 10 years experience in investment banking and 4 years in consulting. Recently he earned Executive MBA degree in HEC, Paris.'
        }
      }
    },
    {
      hash: '#sergei-kouzmin',
      ru: {
        name: 'Сергей Кузьмин',
        title: 'Со-основатель, CPO',
        photo: `../../kuzmin.png`,
        color: `rgba(11,120,191,0.15)`,
        contacts: `../../vcf/rus-serge.vcf`,
        details: {
            title: 'Со-основатель, CPO',
            links: [
              {
                icon: '../../linkedin.png',
                url: 'https://www.linkedin.com/in/sergei-kouzmin-0156251a9/'
              },
              {
                icon: '../../fb.png',
                url: 'https://www.facebook.com/sergey.kouzmin.3'
              }
            ],
            about: 'В Upfinity Сергей занимается разработкой новых продуктов, а также участием в  общем управлении компанией. До Upfinity Сергей работал в инвестиционных блоках крупнейших российских банков, где организовал ряд сделок по структурному финансированию общим объемом более $1 млрд. Сергей является выпускником Высшей Школы Экономики и Лондонской Школы Экономики.'
          }
      },
      en: {
        name: 'Sergei Kouzmin',
        title: 'Co-founder, CPO',
        photo: `../../kuzmin.png`,
        color: `rgba(11,120,191,0.15)`,
        contacts: `../../vcf/eng-serge.vcf`,
        details: {
            title: 'Co-founder, CPO',
            links: [
              {
                icon: '../../linkedin.png',
                url: 'https://www.linkedin.com/in/sergei-kouzmin-0156251a9/'
              },
              {
                icon: '../../fb.png',
                url: 'https://www.facebook.com/sergey.kouzmin.3'
              }
            ],
            about: 'At Upfinity Sergei is focused on new product concept development and also takes part in overall management of the firm. Before Upfinity, Sergei worked at investment divisions of large Russian banks, where he was responsible for originating a series of structured finance deals amounting in total to more than $1 billion. Sergei holds a degree from Higher School of Economics as well as London School of Economics.'
          }
      }
    },
    {
      hash: '#maksim-sitov',
      ru: {
        name: 'Максим Ситов',
        title: 'Со-основатель, COO',
        photo: `../../sitov.png`,
        color: `rgba(214,238,237,1)`,
        contacts: `../../vcf/rus-max.vcf`,
        details: {
          title: 'Со-основатель, COO',
          links: [
            {
              icon: '../../linkedin.png',
              url: 'https://www.linkedin.com/in/maksim-sitov-4bb36944/'
            },
            {
              icon: '../../fb.png',
              url: 'https://www.facebook.com/max.sitov'
            }
          ],
          about: 'Окончил мехмат МГУ. В течение 8 лет занимался производными финансовыми инструментами. В Upfinity работает прежде всего над операционной и технической частью.'
        }
      },
      en: {
        name: 'Maksim Sitov',
        title: 'Co-founder, COO',
        photo: `../../sitov.png`,
        color: `rgba(214,238,237,1)`,
        contacts: `../../vcf/eng-max.vcf`,
        details: {
          title: 'Co-founder, COO',
          links: [
            {
              icon: '../../linkedin.png',
              url: 'https://www.linkedin.com/in/maksim-sitov-4bb36944/'
            },
            {
              icon: '../../fb.png',
              url: 'https://www.facebook.com/max.sitov'
            }
          ],
          about: 'Graduated from Moscow State University with specialization in Mathematics. Worked in financial derivatives for 8 years. At Upfinity focuses primarily on operational and technical stuff.'
        }
      }
    },
    {
      hash: '#danila-ivanchenko',
      ru: {
        name: 'Данила Иванченко',
        title: 'CMO',
        photo: `../../danila.png`,
        color: `rgba(17, 175, 164, 0.15)`,
        contacts: `../../vcf/rus-danila.vcf`,
        details: {
          title: 'CMO',
          links: [
            {
              icon: '../../linkedin.png',
              url: 'https://www.linkedin.com/in/danila-ivanchenko-47b621a6/'
            },
            {
              icon: '../../fb.png',
              url: 'https://www.facebook.com/danila.ivanchenko.1'
            }
          ],
          about: 'Данила окончил кафедру маркетинга в РАНХиГС, занимался кросс-медийным планированием для клиентов и развитием Adtech решений в международных агентствах GroupM и BBDO. Сейчас Данила отвечает за формирование и реализацию маркетинговой стратегии Upfinity.'
        }
      },
      en: {
        name: 'Danila Ivanchenko',
        title: 'CMO',
        photo: `../../danila.png`,
        color: `rgba(17, 175, 164, 0.15)`,
        contacts: `../../vcf/eng-danila.vcf`,
        details: {
          title: 'CMO',
          links: [
            {
              icon: '../../linkedin.png',
              url: 'https://www.linkedin.com/in/danila-ivanchenko-47b621a6/'
            },
            {
              icon: '../../fb.png',
              url: 'https://www.facebook.com/danila.ivanchenko.1'
            }
          ],
          about: 'Danila has a marketing degree at Russian Presidential Academy of National Economy and Public Administration. Engaged in cross-media planning for clients and development of Adtech solutions at international agencies GroupM and BBDO. Danila is now responsible for the formation and implementation of Upfinity’s marketing strategy.'
        }
      }
    },
    {
      hash: '#dmytryj-velykorodnyj',
      en: {
        name: 'Dmytryj Velikorodnyj',
        title: 'Back-end developer',
        photo: `../../velikorodnyj.png`,
        color: `#F8E3DD`,
        details: {
          title: 'Back-end developer',
          links: [
            {
              icon: '../../linkedin.png',
              url: 'https://www.linkedin.com/in/dmytryj-velykorodnyj-490939173/'
            }
          ],
          about: 'At Upfinity Dmytryj is developing and supporting various internal services, starting from a primary app and ending with Upfinity’s AI. He previously worked at a web applications vendor company where he took part in multiple international projects. Dmytryj has more than 6 years of experience in back-end development.'
        }
      },
      ru: {
        name: 'Дмитрий Великородный',
        title: 'Back-end разработчик',
        photo: `../../velikorodnyj.png`,
        color: `#F8E3DD`,
        details: {
          title: 'Back-end разработчик',
          links: [
            {
              icon: '../../linkedin.png',
              url: 'https://www.linkedin.com/in/dmytryj-velykorodnyj-490939173/'
            }
          ],
          about: 'В Upfinity Дмитрий развивает и поддерживает внутренние сервисы, начиная от основного приложения и заканчивая Искусственным Интеллектом. Ранее работал в компании-вендоре веб-приложений, за плечами участие в развитии нескольких международных проектов. Опыт back-end разработки — более 6 лет.'
        }
      }
    },
    {
      hash: '#stanislav-baturin',
      en: {
        name: 'Stanislav Baturin',
        title: 'Front-end developer',
        photo: `../../baturin.png`,
        color: `#D6EEED`,
        details: {
          title: 'Front-end developer',
          links: [
            {
              icon: '../../linkedin.png',
              url: 'https://www.linkedin.com/in/stanislav-baturin/'
            }
          ],
          about: 'At Upfinity Stanislav is developing web applications. Prior to that, Stanislav was involved in the development of applications for the fintech industry. General development experience: 8 years. Graduated from Tomsk State University of Control Systems and Radioelectronics. Engineer\'s degree, “Software for computers and automated systems 230105”.'
        }
      },
      ru: {
        name: 'Станислав Батурин',
        title: 'Front-end разработчик',
        photo: `../../baturin.png`,
        color: `#D6EEED`,
        details: {
          title: 'Front-end разработчик',
          links: [
            {
              icon: '../../linkedin.png',
              url: 'https://www.linkedin.com/in/stanislav-baturin/'
            }
          ],
          about: 'В Upfinity Станислав занимается разработкой веб-приложений. До этого Станислав занимался разработкой приложений для финтех отрасли. Общий опыт разработки: 8 лет. Окончил ТУСУР по специальности “Программное обеспечение ВТ и АС“.'
        }
      }
    },
    {
      hash: '#viachaslau-viarbitski',
      en: {
        name: 'Viachaslau Viarbitski',
        title: 'UX/UI Engineer',
        photo: `../../verbarin.png`,
        color: `#D5E6F1`,
        details: {
          title: 'UX/UI Engineer',
          links: [
            {
              icon: '../../linkedin.png',
              url: 'https://www.linkedin.com/in/vvverbitski/?locale=en_US'
            }
          ],
          about: 'At Upfinity Viachaslau is researching User Experience and is designing interfaces. Has over 7 years experience in developing complex platforms and applications.'
        }
      },
      ru: {
        name: 'Вячеслав Вербицкий',
        title: 'UX/UI инженер',
        photo: `../../verbarin.png`,
        color: `#D5E6F1`,
        details: {
          title: 'UX/UI инженер',
          links: [
            {
              icon: '../../linkedin.png',
              url: 'https://www.linkedin.com/in/vvverbitski/'
            }
          ],
          about: 'В Upfinity занимается исследованием пользовательского опыта и проектированием интерфейсов. До этого 7 лет занимался разработкой сложных платформ и приложений.'
        }
      }
    }
  ]);
  const [currentMember, setCurrentMember] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);

  const openModal = (member) => {
    setCurrentMember(member);
    setModalOpen(true);
  }

  const closeModal = () => {
    setModalOpen(false);
  };

  useEffect(() => {
    const memberHashed = members.filter(item => item.hash == location.hash)
    if(memberHashed.length > 0) {
      openModal(memberHashed[0]['ru'])
    }
  }, [])

  return (
    <Layout>
      <Head>
        <title>Upfinity | Наша команда | Upfinity.io</title>
      </Head>
      <div>
        <h1 className="text-center mt-5 mb-5">Встречайте команду Upfinity</h1>
        
        <div className="container">
        {
          members.map((member, index) => {
            return (
              <button className="card-person column col-4 col-md-6 col-xs-12 mb-3 cursor-pointer" onClick={() => openModal(member['ru'])} onKeyUp={() => {}} key={`member_${index}`}>
                <div className="card">
                    <div className="card-image" style={{ backgroundColor: `${member['ru'].color}` }}>
                        <div className="team-img" style={{backgroundImage: `url(${member['ru'].photo})`}}></div>
                    </div>
                    <div className="card-header">
                        <div className="card-title h5 text-center">{member['ru'].name}</div>
                        <div className="card-subtitle h5 text-center">{member['ru'].title}</div>
                    </div>
                </div>
              </button>
            )
          })
        }
        </div>
      </div>

      <Modal
          isOpen={modalOpen}
          onRequestClose={closeModal}
          style={modalStyles}
          contentLabel="Modal"
          closeTimeoutMS={500}
        >
          {
            currentMember ? (
              <div>
                <div className="modal_thumb__wrapper" style={{backgroundColor: currentMember.color}}>
                  <img src={currentMember.photo} className="team_photo__big" alt={currentMember.name} />
                </div>
                <div className="modal__body">
                  <button className="model__close" onClick={closeModal}><i className="icon icon-cross"></i></button>
                  <div className="modal__name">{currentMember.name}</div>
                  <div className="modal__title">{currentMember.details.title}</div>

                  {
                    currentMember.details.links.map((link, index) => {
                      return (
                        <a target="_blank" rel="noreferrer" href={link.url} key={`link_${index}`} ><img src={link.icon} className="member_link" alt={currentMember.name} /></a>
                      )
                    })
                  }

                  <div>
                  {
                    currentMember.contacts ? (
                      <a href={currentMember.contacts} className="btn mt-3" target="_blank" rel="noreferrer">Сохранить контакт</a>
                    ) : null
                  }
                  </div>

                  <div className="modal__about">{currentMember.details.about}</div>
                </div>
              </div>
            ) : ''
          }
        </Modal>
    </Layout>
  )
}
