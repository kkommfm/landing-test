import React, { useEffect } from 'react';
import '../styles/globals.scss'
// import { appWithTranslation } from 'next-i18next';
import { Auth0Provider } from '@auth0/auth0-react';
import TagManager from "react-gtm-module";

const tagManagerArgs = {
  gtmId: "GTM-WV3GZ4C",
}

const MyApp = ({ Component, pageProps }) => {
  useEffect(() => {
    TagManager.initialize(tagManagerArgs)
  }, []);

  return (<Auth0Provider
      domain={'dev-lyyh4pu2.eu.auth0.com'}
      clientId={'XvemUY784BZc38iXMK5ow9tRibvb7Y1l'}
      redirectUri={'http://localhost:8000/'}>
        <Component {...pageProps} />
      </Auth0Provider>
    );
}

export default MyApp;
// export default appWithTranslation(MyApp);
