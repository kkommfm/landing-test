import React, { useEffect, useState } from 'react';
import axios from "axios";
import Head from 'next/head';
import Link from 'next/link';
import Layout from "../components/layout";
// import { useTranslation, i18n } from 'next-i18next';
import { useRouter } from 'next/router';
import HeroAnimatedBird from '../components/HeroAnimatedBird'
import HeroAnimatedHand from '../components/HeroAnimatedHand'
// import nextI18NextConfig from '../next-i18next.config';

// import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

import logEvent from '../utils/logger';

// export const getStaticProps = async ({ locale }) => ({
//   props: {
//     ...(await serverSideTranslations(locale, ['common'], nextI18NextConfig)),
//   },
// })

const TryDemo = (props) => {
  // const { t, i18n } = useTranslation();
  const [disabled, setDisabled] = useState(false);

  const go = () => {
    setDisabled(true);
    logEvent(33, 'landing3_start', {
      button_clicked: 'landing3_start',
      first_name: null,
      email: null,
      lang: 'ru'
    }, props.token, 'ru')
      .then(() => {
          // if(this.props.intl.locale == 'ru') window.location = `https://app.upfinity.io/ru`;
          // else window.location = `https://app.upfinity.io/en`;
          window.location = `https://app.upfinity.io/ru`;
      })
        .catch(e => {
          console.log(e);
          window.location = `https://app.upfinity.io/ru`;
      })
  }

  return (
    <button disabled={disabled} onClick={go} className="btn btn-xl btn-primary mt-12">Старт...</button>
  )
}

const GoStart = (props) => {
  // const { t, i18n } = useTranslation();
  const [disabled, setDisabled] = useState(false);

  const go = () => {
    setDisabled(true);
    logEvent(34, 'landing3_launch-your-product', {
      button_clicked: 'landing3_launch-your-product',
      first_name: null,
      email: null,
      lang: 'ru'
    }, props.token, 'ru')
      .then(() => {
          // if(this.props.intl.locale == 'ru') window.location = `https://app.upfinity.io/ru`;
          // else window.location = `https://app.upfinity.io/en`;
          window.location = `https://app.upfinity.io/ru`;
      })
        .catch(e => {
          console.log(e);
          window.location = `https://app.upfinity.io/ru`;
      })
  }

  return (
    <button disabled={disabled} onClick={go} className="btn btn-xl btn-primary">Запустить собственный продукт!</button>
  )
}

export default function Home() {
  // const { t } = useTranslation();
  const router = useRouter();
  const [token, setToken] = useState();

  useEffect(() => {
    window.addEventListener('scroll', function() {
        const navbar = document.getElementById('navbar')

        if(navbar) {
            if(window.pageYOffset > 50) {
                navbar.classList.add('shadow-sm')
                navbar.classList.remove('py-6')
                navbar.classList.add('py-1')
            } else {
                navbar.classList.remove('shadow-sm')
                navbar.classList.remove('py-1')
                navbar.classList.add('py-6')
            }
        }
    });

    handleToken();
  }, []);

  const handleToken = () => {
      let cookieToken = getCookieValue('basic-initial-token');

      if(cookieToken && cookieToken.trim() != '') {
          setToken(cookieToken)
      } else {
          const replacedLocation = window.location.toString().replace('#demo', '');
          const newUrl = new URL(replacedLocation);
          const urlParams = new URLSearchParams(newUrl.search);
          const utms = {};

          urlParams.get('utm_source') != null ? utms.utm_source = urlParams.get('utm_source') : ''
          urlParams.get('utm_medium') != null ? utms.utm_medium = urlParams.get('utm_medium') : ''
          urlParams.get('utm_campaign') != null ? utms.utm_campaign = urlParams.get('utm_campaign') : ''
          urlParams.get('utm_term') != null ? utms.utm_term = urlParams.get('utm_term') : ''
          urlParams.get('utm_content') != null ? utms.utm_content = urlParams.get('utm_content') : ''

          axios.post('https://upfinity-proxy.herokuapp.com/cl/tokens/', {
              lang: 'ru',
              data: Object.keys(utms).length === 0 ? {} : {
                  utm: utms
              }
          })
              .then(response => {
                  if(response.data.success) {
                      document.cookie = "basic-initial-token=" + response.data.result;
                      setToken(response.data.result)
                  }
              })
              .catch(e => console.log(e))
      }
  }

  const getCookieValue = (name) => (
      document.cookie.match('(^|;)\\s*' + name + '\\s*=\\s*([^;]+)')?.pop() || ''
  )

  return (
    <Layout>
      <Head>
        <title>Upfinity | Превратите идею в стартап! | Upfinity.io</title>
      </Head>
      <div className="hero_custom">
        <HeroAnimatedHand>
          <img src="/assets/svg/hand_1.svg" alt="" className="hand_1"/>
        </HeroAnimatedHand>
        <HeroAnimatedBird />
      </div>

      <div className="container mt-24 pt-12 pb-12 position-relative mt-0-mobile">
        <h1 className="main_hero text-center">Превращайте идеи в стартапы.<br />Быстро. Удобно. Технологично</h1>

        <div className="subtitle">Ваш персональный продакт менеджер на основе Искусственного Интеллекта. Освободитесь от рутины — сконцентрируйтесь на решениях</div>

        <div className="text-center">
          <TryDemo token={token} />
        </div>
      </div>

      <div className="h1 text-center mt-5 how_it_works">Как это работает<br />на простом примере</div>

      <div className="container mt-6">
        <div className="columns">
          <div className="col-6 col-sm-12">
            <img src="/desk.png" alt="" className="m-w-100 pr-5" />
          </div>
          <div className="col-5 col-sm-12 d-flex align-items-center">
            <div className="classic_text">Пользователь хочет создать <span>технологический стартап</span> в сфере, например, образования</div>
          </div>
        </div>

        <div className="pre-relative mb-6 pre-slim">
          <div className="pre-arrow"></div>
          <div className="text-center mt-5">
            <div className="slim_text">В голове нет четкого продукта,<br />“секретных соусов”, киллер-фич и прочего.</div>
            <div className="slim_text mt-5">Я даже не знаю, <span>чему именно мой стартап будет обучать!</span></div>
          </div>
        </div>



        <div className="h1 text-center">Мы поможем оценить идеи до того,<br /> как вы начнете тратить своё время и деньги</div>

        <div className="begin"><div className="begin_digital">1</div>Для начала необходимо выбрать индустрии, которые вас интересуют</div>
      </div>

      <div className="columns px-5 pre-relative">
        <div className="pre-arrow"></div>
        <div className="col-2 col-lg-3 col-md-4 col-sm-6">
          <div className="item">Administrative Services</div>
          <div className="item">Advertising</div>
          <div className="item">Agriculture and Farming</div>
          <div className="item">Apps</div>
          <div className="item">Artificial Intelligence</div>
          <div className="item">Internet Services</div>
        </div>
        <div className="col-2 col-lg-3 col-md-4 col-sm-6">
          <div className="item">Biotechnology</div>
          <div className="item">Clothing and Apparel</div>
          <div className="item active-item"> <i className="icon icon-check"></i> Commerce and Shopping</div>
          <div className="item">Community and Lifestyle</div>
          <div className="item">Consumer Electronics</div>
          <div className="item">Music and Audio</div>
        </div>
        <div className="col-2 col-lg-3 col-md-4 col-sm-6">
          <div className="item">Consumer Goods</div>
          <div className="item">Content and Publishing</div>
          <div className="item">Navigation and Mapping</div>
          <div className="item">Other</div>
          <div className="item">Payments</div>
          <div className="item">Messaging and Telecommunications</div>
        </div>
        <div className="col-2 col-lg-3 col-md-4 col-sm-6">
          <div className="item">Platforms</div>
          <div className="item">Privacy and Security</div>
          <div className="item">Professional Services</div>
          <div className="item">Real Estate</div>
          <div className="item active-item"> <i className="icon icon-check"></i> Sales and Marketing</div>
          <div className="item">Media and Entertainment</div>
        </div>
        <div className="col-2 col-lg-3 col-md-4 col-sm-6">
          <div className="item">Science and Engineering</div>
          <div className="item">Software</div>
          <div className="item">Events</div>
          <div className="item">Financial Services</div>
          <div className="item">Food and Beverage</div>
          <div className="item">Manufacturing</div>
        </div>
        <div className="col-2 col-lg-3 col-md-4 col-sm-6">
          <div className="item active-item"> <i className="icon icon-check"></i> Gaming</div>
          <div className="item">Government and Military</div>
          <div className="item">Hardware</div>
          <div className="item">Health Care</div>
          <div className="item">Information Technology</div>
          <div className="item">Lending and Investments</div>
        </div>
      </div>

      <div className="container mt-6">

        <div className="h1 text-center">Upfinity анализирует<br />800 тысяч существующих компаний</div>

        <div className="columns mt-5 pre-relative">
          <div className="pre-arrow"></div>
          <div className="col-8 col-md-12">
            <img src="/chart.png" alt="" className="m-w-100 pr-5"/>
          </div>
          <div className="col-4 col-md-12">
            <h2>Трендовость</h2>
            <p>Популярность отрасли по отношению к другим  </p>

            <h2>Финансовые барьеры</h2>
            <p>Насколько сложно финансово “войти” в индустрию</p>

            <h2>Технологические барьеры</h2>
            <p>Насколько индустрия развита в техническом плане</p>

            <h2>Размер рынка</h2>
            <p>Имеется ли достаточный рынок для реализации стартапа</p>

            <h2>Конкурентность</h2>
            <p>Много ли конкурентов в данной индустрии</p>
          </div>
        </div>

        <div className="h1 text-center mt-5">предоставляет конкурентный анализ <br />и продуктовые особенности</div>
        <div className="begin"><div className="begin_digital">2</div>Из списка существующих успешных компаний остается выбрать продукты и идеи, которые вам нравятся</div>

        <div className="columns pre-relative">
          <div className="pre-arrow"></div>
          <div className="col-8 col-md-12">
            <img src="/list.png" alt="" className="m-w-100 pr-5" />
          </div>
          <div className="col-4 col-md-12 d-flex align-items-center">
            <div className="step_2">
            Искусственный Интеллект <span>определяет продуктовые “фичи”</span> существующих компаний и помогает сформулировать гипотезу для стартапа на действующих решениях
            </div>
          </div>
        </div>

        <div className="h1 text-center mt-5">В результате проделанной работы<br />сформировалось описание продукта</div>

        <div className="columns pre-classic">
          <div className="col-6 col-md-12 d-flex align-items-center">
            <div className="classic_text">
              <div>Стартап <span>будет помогать студентам учить иностранные языки с помощью ИИ!</span> </div>

              <div className="mt-4">Основными фишками сервиса будут: облачная инфраструктура, персонализированный подход к обучению и умная лента рекомендаций</div>
            </div>
          </div>
          <div className="col-6 col-md-12">
            <img src="/result.png" alt="" className="m-w-100 mt-4 pr-5" />
          </div>
        </div>

        <div className="h1 text-center mt-5">Так схематично выглядит работа с Upfinity</div>

        <div className="text-center mt-4 mb-6">
          <GoStart token={token} />
        </div>
      </div>
    </Layout>
  )
}
