import Head from 'next/head'
import styles from '../../styles/Home.module.css'
import Link from 'next/link'
import Layout from "../../components/layout";
// import nextI18NextConfig from '../../next-i18next.config';

// import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

// export const getStaticProps = async ({ locale }) => ({
//   props: {
//     ...await serverSideTranslations(locale, ['common'], nextI18NextConfig),
//   },
// })

export default function Home() {
  return (
    <Layout>

      <div className="container">
            <h1 className="text-center my-12">Сервисы и Тарифы</h1>
            <div>
            <p>Настоящий документ (далее “Сервисы и Тарифы”) является приложением к Пользовательскому соглашению сайта upfinity.io между индивидуальным предпринимателем Аношиным Денисом Геннадьевичем, ОГРНИП 321774600061797 и Пользователями, размещенному в сети Интернет по адресу <Link href="/legal/rules">https://upfinity.io/legal/rules</Link> (далее “Соглашение”). Все термины, которые в явном виде не определены в Сервисах и Тарифах, имеют значение, указанное в Соглашение.</p>

            <p>Целью Сервисов и Тарифов является публикация списка Сервисов, а также платных продуктов и услуг, покупка которых доступна Пользователям через Сайт и Сервисы с указанием актуальных цен. На все продукты и услуги, указанные в Сервисах и Тарифах, распространяются действия Соглашения и Политики конфиденциальности (<Link href="/legal/confidential">https://upfinity.io/legal/confidential</Link>), а также правила пользования конкретного сервиса, если они размещены на Сайте.</p>

            <p>В случае, если цена сервиса указана более чем в одной валюте, Пользователь имеет право выбрать любой из вариантов. При оплате в валюте, отличной от валюты, в которой указана цена, будет произведена автоматическая конвертация по курсу Сайта или платежного сервиса, используемого на Сайте или соответствующем сервисе.</p>

            <p>Во всех случаях, когда указываются электронные адреса Сервисов, указываются адреса стартовых страниц, при этом сами Сервисы могут иметь вложенные страницы, являющиеся неотъемлемой частью соответствующих Сервисов. Электронные адреса всех вложенных страниц Сервиса состоят из адреса стартовой страницы с добавлением уникального адреса вложенной страницы.</p>

            <p>Список Сервисов:</p>

            <p><b>1. Котел идей</b></p>

            <p>Рабочее пространство, в котором Пользователь может составлять выборки компаний для анализа из базы данных компаний Оператора, с учетом возможностей поиска, предусмотренных интерфейсом Сервиса.</p>

            <p>Сервис доступен как для зарегистрированных, так и для незарегистрированных Пользователей. Функционал Сервиса для зарегистрированных и незарегистрированных Пользователей может отличаться (зарегистрированным Пользователям могут быть доступны возможности сервиса, не доступные незарегистрированным Пользователям). </p>

            <p>Электронный адрес Сервиса: <a target="_blank" rel="noreferrer" href={`https://app.upfinity.io/idea-soup/`}>https://app.upfinity.io/idea-soup/</a></p>

            <p>Платные продукты и услуги, относящиеся к Котлу идей:</p>

            <table className="border">
              <tbody>
                <tr>
                  <td>№</td>
                  <td>Название</td>
                  <td>Описание продукта / услуги</td>
                  <td>Порядок оплаты</td>
                  <td>Цена</td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Отчет по Котлу идей</td>
                  <td>
                    <p>Отчет с аналитикой по 20 компаниям из выборки Пользователя в рамках Котла идей с наивысшим Upfinity score.</p>
                    <p>В составе аналитики по каждой компании выборки входят данные, указанные на заключительном экране Котла идей, а также дополнительная инфомация.</p>
                  </td>
                  <td>Единовременно</td>
                  <td>200 рублей </td>
                </tr>
              </tbody>
            </table>

            <p><b>2. Сообщество (“Комьюнити”)</b></p>

            <p>Система для дистанционного общения между Пользователями. Пользователям могут быть доступны:</p>

            <p>
              <ul>
              <li>Текстовые чаты 1 на 1 между Пользователями</li>

              <li>Групповые текстовые чаты с ограниченным количеством Пользователей</li>

              <li>Открытые текстовые чаты (“каналы”) с большим количеством Пользователей, модерируемые Оператором</li>

              <li>Звонки 1 на 1</li>

              <li>Групповые звонки с ограниченным количеством Пользователей</li>
              </ul>
            </p>

            <p>В звонках может быть предусмотрена передача видео (“видео-звонки”).</p>

            <p>Сервис доступен исключительно зарегистрированным Пользователям.</p>

            <p>Электронные адресы Сервиса:</p>

            <p>
              <ul>
                <li>Для текстовых чатов:  <a target="_blank" rel="noreferrer" href={`https://app.upfinity.io/chat/`}>https://app.upfinity.io/chat/</a></li>

                <li>Для звонков: <a target="_blank" rel="noreferrer" href={`https://app.upfinity.io/calls/`}>https://app.upfinity.io/calls/</a></li>
              </ul>
            </p>

            <p>Платные продукты и услуги, относящиеся к Комьюнити: не предусмотрены</p>
            </div>
        </div>
    </Layout>
  )
}
