import { handleAuth, handleLogin } from '@auth0/nextjs-auth0';

export default handleAuth({
  async login(req, res) {
    try {
       // Pass in custom params to your handler
      await handleLogin(req, res, { authorizationParams: { customParam: 'foo' } });
    } catch (error) {
      // Add you own custom error logging.
      res.status(error.status || 500).end(error.message);
    }
  }
});